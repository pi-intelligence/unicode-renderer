package de.pi_intelligence.pdfembedded;

import sun.awt.image.BufferedImageGraphicsConfig;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by moe on 06/04/16.
 */
public class Renderer {

  private Font utf8Font = null;
  private Font temp = null;

  public Renderer() {
    try {
      utf8Font = Font.createFont(Font.TRUETYPE_FONT, getUTF8FontPath()).deriveFont(20f);
    } catch (FontFormatException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  private static final String UTF8_FONT_NAME = "/de/pi_intelligence/pdfembedded/arial-unicode.ttf";

  private File getUTF8FontPath() {
    try {
      return new File(getClass().getResource(UTF8_FONT_NAME).toURI());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }

    return null;
  }


  public void drawText(Graphics out, Text text) {


    if (text.isUTF8()) {
      temp = out.getFont();
      out.setFont(utf8Font);
      out.drawString(text.getContent(), text.getX(), text.getY());
      out.setFont(temp);
    } else {
      out.drawString(text.getContent(), text.getX(), text.getY());
    }


  }

  public static void main(String[] args) {
    BufferedImage image = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_ARGB);
    Graphics out = image.getGraphics();
    out.setColor(Color.black);
    out.setFont(out.getFont().deriveFont(20f));
    Renderer renderer = new Renderer();
    renderer.drawText(out, new Text(500, 500, "asdasd asdasd اسداسداسد ساداسد اسداسد"));
    renderer.drawText(out, new Text(500, 600, "faksdklsajdalsdnasdasda"));
    renderer.drawText(out, new Text(100, 100, "Ку фалля латины дэмокритум вяш, ан льаборэ ыкчпэтында аппэльлььантюр вим. Дёко вивэндюм дигнижжим нэ ыам. Модо ельлюд волютпат ат вяш. Ад фабулаз плььатонэм вим, чент латины дыфинитеоным еюж ку, эи эжт дёжкэрэ мэнандря. Оффекйяж лобортис пытынтёюм вяш ку, дэбыт чонэт пожжёт зыд ад.\n"));
    renderer.drawText(out, new Text(100, 200,"四見止断断分正事録北大私程武頂告周育窓。面産報了写所済意府鈍勝稿激社。求買日炭儀規緑来文怖鹿全写。字本木件組足寿帰身兜食直期厚暮記負視。道早面価障月品学利済当咲応。読間身曲急小位百雪景午里重合回無捨壌図。歳選政集番芸政料頭号官予変見年。"));
    try {
      ImageIO.write(image, "PNG", new File("out/temp.png"));
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}
