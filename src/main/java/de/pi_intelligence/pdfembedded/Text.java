package de.pi_intelligence.pdfembedded;

import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created by moe on 06/04/16.
 */
public class Text {
  private int x = 0;

  public Text(int x, int y, String content) {
    this.x = x;
    this.y = y;
    this.content = content;
  }

  private int y = 0;
  private String content = "";

  private static final CharsetEncoder encoder = StandardCharsets.ISO_8859_1.newEncoder();

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }


  public boolean isUTF8() {

    return !encoder.canEncode(content);
  }
}
